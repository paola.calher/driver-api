-- Database: driver

-- DROP DATABASE IF EXISTS driver;

CREATE DATABASE driver
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_Australia.1252'
    LC_CTYPE = 'English_Australia.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE SCHEMA IF NOT EXISTS api;	

CREATE SEQUENCE IF NOT EXISTS api.person_id_seq;
CREATE TABLE IF NOT EXISTS api.persons (
	person_id 		serial PRIMARY KEY,
	identification  VARCHAR ( 50 ) UNIQUE NOT NULL,
	name 			VARCHAR ( 200 ) NOT NULL,
	last_name		VARCHAR ( 200 ) NOT NULL,
	email 			VARCHAR ( 255 ) UNIQUE NOT NULL,
	created_on 		TIMESTAMP NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS api.rol_id_seq;
CREATE TABLE IF NOT EXISTS api.roles(
   role_id 		serial PRIMARY KEY,
   role_name 	VARCHAR (255) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS api.person_roles (
  person_id 	INT NOT NULL,
  role_id 		INT NOT NULL,
  grant_date 	TIMESTAMP,
  PRIMARY KEY (person_id, role_id),
  FOREIGN KEY (role_id)
      REFERENCES api.roles (role_id),
  FOREIGN KEY (person_id)
      REFERENCES api.persons (person_id)
);