package com.calher.driver.dao;

import java.util.List;
import java.util.Optional;
import com.calher.driver.model.IModel;

public interface IDao<T extends IModel> {
	/**
	 * Returns an entity according to its identifier.
	 * @param identifier
	 * @return
	 */
	Optional<T> get(Class<T> entity, long identifier);
	
	/**
	 * Returns all existing entities.
	 * @return
	 */
	List<T> getAll(Class<T> entity);
	
	/**
	 * Saves an entity.
	 * @param entity
	 */
	void save(T entity);
	
	/**
	 * Deletes an entity.
	 * @param entity
	 */
	void delete(T entity);
}
