package com.calher.driver.dao;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.calher.driver.model.IModel;

public class BaseDao<T extends IModel> implements IDao<T> {
	
protected static final EntityManager entityManager;
	
	static {
		entityManager = Persistence.createEntityManagerFactory("driver-api").createEntityManager();
	}
	
	public Optional<T> get(Class<T> entity, long identifier) {
		return Optional.ofNullable(entityManager.find(entity, identifier));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll(Class<T> entity) {
		Query query = entityManager.createQuery("SELECT e FROM " + entity.getSimpleName() + " e");
		return Collections.checkedList(query.getResultList(), entity);
	}

	@Override
	public void save(T entity) {
		if(entity.getIdentifier() == null) {
			executeTransaction(entityManager -> entityManager.persist(entity));
		} else {
			executeTransaction(entityManager -> entityManager.merge(entity));
		}
	}
	
	@Override
	public void delete(T entity) {
		executeTransaction(entityManager -> entityManager.remove(entity));
		
	}
	
	
	/**
	 * Executes a transaction.
	 * @param action
	 */
	private void executeTransaction(Consumer<EntityManager> action) {
		EntityTransaction tx = entityManager.getTransaction();
		try {
			tx.begin();
			action.accept(entityManager);
			tx.commit();
		} catch (RuntimeException e) {
			tx.rollback();
			throw e;
		}
	}
}
