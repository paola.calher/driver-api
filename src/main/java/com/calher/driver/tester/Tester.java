package com.calher.driver.tester;

import com.calher.driver.dao.BaseDao;
import com.calher.driver.model.Rol;

public class Tester {

	public static void main(String[] args) {
		BaseDao baseDao= new BaseDao();
		Rol rol = new Rol();
		rol.setRoleName("Driver");
		baseDao.save(rol);
		System.out.println(baseDao.getAll(Rol.class));
	}
}
