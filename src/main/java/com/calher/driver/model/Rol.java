package com.calher.driver.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "roles", schema = "api")
public class Rol implements IModel{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="rol_seq_generator")
	@SequenceGenerator(name = "rol_seq_generator", sequenceName = "api.rol_id_seq", allocationSize = 1)
	@Column(name = "role_id")
	private long rolId;
	
	@Column(name = "role_name")
	private String roleName;

	public long getRolId() {
		return rolId;
	}

	public void setRolId(long rolId) {
		this.rolId = rolId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	

	@Override
	public Object getIdentifier() {
		return this.rolId;
	}

	@Override
	public String toString() {
		return "Rol [rolId=" + rolId + ", roleName=" + roleName + "]";
	}

	
	
}
