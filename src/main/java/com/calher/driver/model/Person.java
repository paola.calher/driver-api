package com.calher.driver.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "persons", schema = "api")
public class Person implements IModel{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator ="person_seq_generator")
	@SequenceGenerator(name = "person_seq_generator", sequenceName = "api.person_id_seq", allocationSize = 1)
	@Column(name = "person_id")
	private long personId;
	private String identification;
	private String name;
	
	@Column(name = "last_name")
	private String lastName;
	private String email;
	
	@Column(name = "created_on")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdOn;

	public long getPersonId() {
		return personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public Object getIdentifier() {
		return this.personId;
	}

	@Override
	public String toString() {
		return "Person [personId=" + personId + ", identification=" + identification + ", name=" + name + ", lastName="
				+ lastName + ", email=" + email + ", createdOn=" + createdOn + "]";
	}
	
	
}
