package com.calher.driver.model;

public interface IModel {

	public abstract Object getIdentifier();
}
